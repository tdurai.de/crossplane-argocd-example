# Crossplane example

Project in IP-phase 03/2024 - [Introduction to Crossplane](https://www.crossplane.io/)

Deploy and configure Crossplane in an existing K8s cluster.

Create some infrastructure in Google Cloud
(Order a Google Account [here](https://cloud.google.com/free) - 300$ free credits).

Based on [video](https://www.youtube.com/watch?v=n8KjVmuHm7A).

## Prerequisites

### install tools

* [Helm](https://helm.sh/docs/intro/install/)
* [kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/)
* git
* [Minikube](https://minikube.sigs.k8s.io/docs/start/) (or a different K8s platform) && [Docker](https://docs.docker.com/engine/install/)
* [K9S](https://k9scli.io/topics/install/) (optional)

## deploy Crossplane

[`minikube`@`GCP` example](https://docs.crossplane.io/latest/getting-started/provider-gcp/)

```bash
# clone git repo
git clone https://gitlab.com/tdurai.de/crossplane-example.git
cd crossplane-example
```

```bash
# start K8s
minikube start
```

```bash
# prepare Helm repo
helm repo add crossplane-stable https://charts.crossplane.io/stable
helm repo update
```

```bash
# deploy Crossplane
helm install crossplane \
crossplane-stable/crossplane \
--namespace crossplane-system \
--create-namespace
```

```bash
# verify
kubectl get pods -n crossplane-system
kubectl api-resources | grep crossplane
```

## Create a Kubernetes secret for GCP

Needs to be done manually. It includes a personal secret.

[Generate a GCP service account JSON file.](https://cloud.google.com/iam/docs/keys-create-delete)
Save this json file as `gcp-credentials.json`. (Defined in `.gitignore`. Here be dragons!)

```bash
# create
kubectl create secret \
generic gcp-secret \
-n crossplane-system \
--from-file=creds=./gcp-credentials.json

# verify
kubectl describe secret -n crossplane-system gcp-secret
```

## configure Crossplane with GCP as cloud provider (use private account)

[Docs](https://docs.crossplane.io/latest/getting-started/provider-gcp/#install-the-gcp-provider)

```bash
# install GCP provider
kubectl apply -f crossplane/provider.yaml

# verify
kubectl get providers
```

```bash
# create GCP provider config
# !!! You have to modify the projectID in the file !!!
kubectl apply -f crossplane/providerConfig.yaml

# verify
kubectl get providerconfig
```

## deploy example resource - storage bucket

```bash
# deploy bucket
# !!! You have to modify the name of the bucket in the file !!!
kubectl apply -f crossplane/storageBucket.yaml

# verify
kubectl get bucket
```

Check Bucket in [Google Cloud Console](https://console.cloud.google.com).

## deploy example resource - GKE cluster

```bash
# deploy cluster
kubectl apply -f crossplane/gkeCluster.yaml

# verify
kubectl get cluster
```

Check GKE cluster in [Google Cloud Console](https://console.cloud.google.com).

## deploy example resource via Helm

```bash
# deploy provider
kubectl apply -f crossplane/providerHelm.yaml

# deploy provider config
kubectl apply -f crossplane/providerConfigHelm.yaml

# deploy Helm chart
kubectl apply -f crossplane/helloWorldHelm.yaml

# verify
# When verifiying be aware to use the APIs defined in the yaml files.
kubectl get Release.helm.crossplane.io
```
